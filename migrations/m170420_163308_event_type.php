<?php

use yii\db\Migration;

class m170420_163308_event_type extends Migration
{
    public function up()
    {
    $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%event_type}}', [
            'id' => $this->primaryKey(),
            'event_id' => $this->smallInteger()->notNull(),           
            'type_name' => $this->string()->notNull(),
            'type_image' => $this->string()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%event_type}}');
    }
}

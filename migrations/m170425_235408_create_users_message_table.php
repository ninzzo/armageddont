<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_message`.
 */
class m170425_235408_create_users_message_table extends Migration
{
    
    public function up()
    {   
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_message}}', [
            'id' => $this->primaryKey(),
            'event_id' => $this->integer()->notNull(), 
            'created_at' => $this->integer()->notNull(),           
            'user' => $this->string()->notNull(),
            'user_avatar' => $this->string()->notNull(),            
            'msg' => $this->string(2000)->notNull(),
        ], $tableOptions);

    }

    public function down()
    {        
        $this->dropTable('{{%user_message}}');
    }

}

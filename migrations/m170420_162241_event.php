<?php

use yii\db\Migration;

class m170420_162241_event extends Migration
{
    public function up()
    {   
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%event}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),           
            'name' => $this->string()->notNull(),
            'type' => $this->smallInteger()->notNull(), 
            'coord_x' => $this->string()->notNull(),
            'coord_y' => $this->string()->notNull(),
            'comment' => $this->string(1000)->notNull(),
        ], $tableOptions);

    }

    public function down()
    {        
        $this->dropTable('{{%event}}');
    }

 
}

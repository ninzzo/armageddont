<?php

use yii\db\Migration;

class m170503_101128_create_table_cron_push extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%cron_push}}', [
            'id' => $this->primaryKey(),           
            'send' => $this->smallInteger()->notNull()->defaultValue(0),
            'event_id' => $this->smallInteger()->notNull(),
            'owner' => $this->string()->notNull(),
            'textpush' => $this->string()->notNull(),
        ], $tableOptions);

   
    }

    public function down()
    {
        $this->dropTable('{{%cron_push}}');
    }
}

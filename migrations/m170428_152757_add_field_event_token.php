<?php

use yii\db\Migration;

class m170428_152757_add_field_event_token extends Migration
{
     public function up()
    {
        $this->addColumn('{{%event}}', 'device_token', $this->string(164));

        
    }

    public function down()
    {
        $this->dropColumn('{{%event}}', 'device_token');
    }
}

<?php

use yii\db\Migration;

class m170420_163249_event_photo extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%event_photo}}', [
            'id' => $this->primaryKey(),
            'event_id' => $this->smallInteger()->notNull(),           
            'photo_name' => $this->string()->notNull(),
            'main' => $this->smallInteger()->notNull()->defaultValue(0),
            
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%event_photo}}');
    }
   
}

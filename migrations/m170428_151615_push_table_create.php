<?php

use yii\db\Migration;

class m170428_151615_push_table_create extends Migration
{
     public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }       

        $this->createTable('{{%push}}', [
            'id' => $this->primaryKey(),
            'device_token' => $this->string()->notNull(),          
            'platform' => $this->smallInteger()->notNull(),            
            
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%push}}');
    }
}

<?php

use yii\db\Migration;

class m170426_133310_table_polygon extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%polygon}}', [
            'id' => $this->primaryKey(),
            'event_id' => $this->smallInteger()->notNull(),           
            'coord_x' => $this->string()->notNull(),
            'coord_y' => $this->string()->notNull(),
            
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%polygon}}');
    }
}

<?php

use yii\db\Migration;

class m170425_161753_add_dangerous_column extends Migration
{    

    public function up()
    {
        
        $this->addColumn('{{%event}}', 'danger_level', $this->smallInteger()->notNull()->defaultValue(1));  

    }

    public function down()
    {
        $this->dropColumn('{{%event}}', 'role');
    }
}

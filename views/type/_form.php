<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\EventType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-type-form">

   
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?php echo $form->errorSummary($model, ['header' => '']); ?>

   

    <?= $form->field($model, 'type_name')->textInput(['maxlength' => true]) ?>

    <?php
    if(Yii::$app->controller->action->id == 'update' and !empty($model->type_image)) { 
            echo $form->field($model, 'imageFile')->widget(FileInput::classname(), 
                    ['pluginOptions' => [

                    'initialPreview'=>[
                        '/web/uploads/' . $model->type_image                     
                    ],
                    'initialPreviewAsData'=>true,
                    'initialCaption'=>$model->type_image,

                        'showCaption' => true,
                        'showRemove' => false,
                        'showUpload' => false,
                        'browseClass' => 'btn btn-primary btn-block',
                        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                        'browseLabel' =>  'Выбрать картинку',
                    ]],
                    ['options' => ['accept' => 'image/*', 'multiple' => false],
            ])->label("Картинка");
    } else {
            echo $form->field($model, 'imageFile')->widget(FileInput::classname(), 
                    ['pluginOptions' => [
                        'showCaption' => true,
                        'showRemove' => false,
                        'showUpload' => false,
                        'browseClass' => 'btn btn-primary btn-block',
                        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                        'browseLabel' =>  'Выбрать картинку',
                    ]],
                    ['options' => ['accept' => 'image/*', 'multiple' => false],
            ])->label("Картинка");

    }               

    ?>

	<br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

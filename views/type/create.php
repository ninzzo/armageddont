<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EventType */

$this->title = 'Создать тип собития';
$this->params['breadcrumbs'][] = ['label' => 'Тип собития', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

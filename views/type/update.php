<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventType */

$this->title = 'Редакрирование типа собития: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Тип события', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редакрирование';
?>
<div class="event-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

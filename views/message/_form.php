<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Event;

/* @var $this yii\web\View */
/* @var $model app\models\UserMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-message-form">

    <?php $form = ActiveForm::begin(); ?>

    

     <?= $form->field($model, 'event_id')->dropdownList(
    Event::find()->select(['name', 'id'])->indexBy('id')->column(),
    ['prompt'=>'Выберите событие']
    ) ?>




    <?= $form->field($model, 'user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_avatar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'msg')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

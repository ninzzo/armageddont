<?php

use yii\helpers\Html;
use yii\grid\GridView;

use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'События';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <p>
        <?= Html::a('Создать событие', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',            
            [
                'filter' => false,
                'attribute' => 'created_at',
                'format' => 'datetime',
                'filterOptions' => [
                    'style' => 'max-width: 180px',
                ],
            ],
            'name', 
            'comment',
            [
                'attribute' => 'coord_x',
                'format' => 'html',    
                'filter' => false,
                'header' => 'Координаты',
                'value' => function ($data) {
                    return $data['coord_x'].', <br>'.$data['coord_y'];
                },
            ],
            [
                'filter' => false,
                'attribute' => 'danger_level',               
                'format' => 'html', 
                'value' => function ($data) {
                    if( $data['danger_level'] == 1 ){return 'Слабый';}
                    if( $data['danger_level'] == 2 ){return 'Средний';}
                    if( $data['danger_level'] == 3 ){return 'Сильный';}    
                    
                },
                
            ],
            [
                'attribute' => 'eventtype.type_name',
                'format' => 'html',    
                'value' => function ($data) {
                    return $data['eventtype']['type_name'].' '.Html::img(Yii::getAlias('@web').'/uploads/'. $data['eventtype']['type_image'],
                        ['width' => '20px']);
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

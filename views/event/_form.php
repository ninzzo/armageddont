<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\EventType;

use kartik\file\FileInput;
use yii\helpers\Url;
use app\models\EventPhoto;
use yii\bootstrap\Modal;
use app\models\Polygon;

/* @var $this yii\web\View */
/* @var $model app\models\Event */
/* @var $form yii\widgets\ActiveForm */
// AIzaSyDiern53s3oclBm52lQK0F-YWzLWCA_5BU
?>

<div class="event-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>   

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>    

    <?= $form->field($model, 'type')->dropdownList(
    EventType::find()->select(['type_name', 'id'])->indexBy('id')->column(),
    ['prompt'=>'Выберите тип']
    ) ?>

 

    <?php echo $form->field($model, 'danger_level')->dropDownList([
    '1' => 'Cлабый',
    '2' => 'Средний',
    '3'=>'Сильный'
    ]);
    ?>






    <?php

    if(Yii::$app->controller->action->id == 'update') { 
        //перенести поиск в контроллер
        $modelPhoto = EventPhoto::find()       
         ->where(['event_id' => $model->id])
         ->all(); 
        if($modelPhoto) {
        echo '<label class="control-label">Загруженные картинки:</label>';
        echo '<div class="clearfix"></div>';
            foreach($modelPhoto as $photo) {
                echo '   
                <div class="file-preview-frame krajee-default  kv-preview-thumb" id="imageid-'.$photo->id.'" data-fileindex="0" data-template="image"><div class="kv-file-content">
                <img src="/web/uploads/' . $photo->photo_name . '" class="file-preview-image kv-preview-data" title="rss.jpg" alt="rss.jpg" style="width:auto;height:160px;">
                </div><div class="file-thumbnail-footer">
                    <div class="file-footer-caption"></div>
                <div class="file-actions">
                    <div class="file-footer-buttons">
                          <button onclick="DeleteImage('.$photo->id.')" type="button" class="kv-file-zoom btn btn-xs btn-default" title="Удалить"><i class="glyphicon glyphicon-trash"></i></button></div>
                    <div class="clearfix"></div>
                </div>
                </div>
                </div>';
            }
        echo '<div class="clearfix"></div>';
        }
    }  
     
    
    echo $form->field($model, 'imageFile[]')->widget(FileInput::classname(), [
    'options' => ['multiple' => true],
    'pluginOptions' => ['previewFileType' => 'any',
                        'showCaption' => false,
                        'showRemove' => false,
                        'showUpload' => false,]
    ]);
    ?>

    <?php  
    echo $form->field($model, 'coord_x')->textInput(['id'=>'x_coord', 'readonly' => true, 'maxlength' => true,]);
    echo '<button onclick="showmap(); return false;" class="btn btn-info btn-xs">Выбрать координаты</button> ';
    echo $form->field($model, 'coord_y')->textInput(['id'=>'y_coord', 'readonly' => true, 'maxlength' => true,]);
    ?>

    


    <?= $form->field($model, 'comment')->textarea(['rows' => 2, 'cols' => 5]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>





 <?php 

     Modal::begin(['id' => 'modal',
        'header' => 'Определение координат метки']);
         echo '<div id="map_canv"></div>';
     Modal::end();
    ?>
   
<?php if(Yii::$app->controller->action->id == 'update') { 
//удалять загруженные ранее фотки можем только в редактировании
  //Все скрипты ниже перенести в registerJS позже
?>

<script>

    function DeleteImage(id) {
        
        $.ajax({
               url: '<?php echo Yii::$app->request->baseUrl. '/event/deleteimage' ?>',
               type: 'post',
               data: {
                         photoId: id, 
                         eventId: <?= $model->id ?>                       
                     },
               success: function (data) {
                  if(data == 'good'){ $('#imageid-' + id).remove(); }
                 
               }
            });
    }
</script>
<?php } ?>

<script>
 function showmap() {
    
    $("#modal").modal("show");
    var $modal = $('#modal'),
    $map = $('#map_canv');
    $modal.on('shown.bs.modal', function () {
      google.maps.event.trigger($map[0], 'resize');        
    });
 }
    
</script>

<script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDiern53s3oclBm52lQK0F-YWzLWCA_5BU" type="text/javascript"></script>

<style type="text/css" >
      #map_canv {            
            height:500px;
     }
</style>


  <script type="text/javascript">

function initialize() {

            var latlng = new google.maps.LatLng(55.751888, 37.620777);
            var map = new google.maps.Map(document.getElementById('map_canv'), {

            <?php if(Yii::$app->controller->action->id == 'update') { 
                echo 'center: new google.maps.LatLng(' . $model->coord_x . '+ 0.1, ' . $model->coord_y . '- 0.2),';
            } else { ?>
                center: new google.maps.LatLng(55.851888, 37.420777),
            <?php } ?>


                zoom: 11,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var marker = new google.maps.Marker({
            <?php if(Yii::$app->controller->action->id == 'update') { 
                echo 'position: new google.maps.LatLng(' . $model->coord_x . ', ' . $model->coord_y . '),';
            } else { ?>
                position: latlng,
            <?php } ?>
                map: map,
                title: 'Set lat/lon values for this property',
                draggable: true
            });

            google.maps.event.addListener(marker, 'dragend', function (event) {
                document.getElementById("x_coord").value = this.getPosition().lat();
                document.getElementById("y_coord").value = this.getPosition().lng();
                alert('Координаты маркера внесены в форму. Можете закрыть модальное окно.');
            });


   <?php

    if(Yii::$app->controller->action->id == 'update') {            
          
 echo  'var triangleCoords = [';



$polygon = Polygon::find()     
     ->where(['event_id' => $model->id])
     ->all();

foreach($polygon as $val) { echo '{lat: '.$val->coord_x.', lng: '.$val->coord_y.'},';}
?>

    /*{lat: 25.774, lng: -80.190},
    {lat: 18.466, lng: -66.118},
    {lat: 32.321, lng: -64.757},
    {lat: 25.774, lng: -80.190}*/
  ];

  // Construct the polygon.
  var bermudaTriangle = new google.maps.Polygon({
    paths: triangleCoords,
    strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#FF0000',
    fillOpacity: 0.35
  });
  bermudaTriangle.setMap(map);

<?php 
}
?>
         
}
google.maps.event.addDomListener(window, "load", initialize);







</script>





    
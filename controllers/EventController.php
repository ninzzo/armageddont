<?php

namespace app\controllers;

use Yii;
use app\models\Event;
use app\models\EventSearch;
use app\models\EventPhoto;
use app\models\Polygon;
use app\models\Push;
use app\models\CronPush;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;



/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends Controller
{   

    public $layout = '@app/views/layouts/admin';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        'access' => [
        'class' => AccessControl::className(),                
        'rules' => [
            [
            'actions' => ['index'],
            'allow' => true,
            'roles' => ['admin'],
            ],
            [
            'actions' => ['view'],
            'allow' => true,
            'roles' => ['admin'],
            ],
            [
            'actions' => ['create'],
            'allow' => true,
            'roles' => ['admin'],
            ],
            [
            'actions' => ['update'],
            'allow' => true,
            'roles' => ['admin'],
            ],
            [
            'actions' => ['delete'],
            'allow' => true,
            'roles' => ['admin'],
            ],
            [
            'actions' => ['fileupload'],
            'allow' => true,
            'roles' => ['admin'],
            ],
            [
            'actions' => ['deleteimage'],
            'allow' => true,
            'roles' => ['admin'],
            ],
            [
            'actions' => ['curlpost'],
            'allow' => true,
            'roles' => ['admin'],
            ],
            [
            'actions' => ['cronpush'],
            'allow' => true,           
            ],
            
        ],
        ],
        ];
    }


    public function actionCronpush() {
        
        $cronpush = CronPush::find()
            ->all();

        foreach($cronpush as $cron) { 

            echo $cron['id'].'<br>';            
            echo $cron['send'].'<br><br>';            
        }
    }


    public function actionCurlpost() {
        
        //upload file by curl
$url_to_file1 = Yii::$app->basePath . '/web/uploads/w450h4001385925290Cloud.png';
$postFields = array(); 
 
//fileshttp://armageddont/web/apievents
 $postFields['event_photos'] = "@$url_to_file1";
//metaData
$postFields['created_at'] = time();
$postFields['name'] = 'hello_moto';
$postFields['coord_x'] = '55.74';
$postFields['coord_y'] = '37.6';
$postFields['type'] = 1;
$postFields['comment'] = "ilon_mask";
$curl_handle = curl_init();
 
curl_setopt($curl_handle, CURLOPT_URL, 'http://backend.armageddont.kingbird.ru/apievents');
curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl_handle, CURLOPT_POST, true);
curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $postFields);  
//execute the API Call
$server_output = curl_exec($curl_handle);
curl_close ($curl_handle);
if ($server_output == "OK") { echo $server_output; } else { echo $server_output; }

        

      
        /*$alltoken = Push::find()->all();
        foreach($alltoken as $t) { 
            echo $t['device_token'].'<br>';
        }*/




        //test create event   
        /*$json = '[{"photo_name":"foto.png","photo_url":"url"},{"photo_name":"foto.png","photo_url":"url"},{"photo_name":"foto.png","photo_url":"url"},{"photo_name":"foto.png","photo_url":"url"},{"photo_name":"foto.png","photo_url":"url"}]';

        $time = time();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"http://armageddont/web/apievents");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "event_photos=$json&created_at=$time&name=testумуте&type=1&coord_x=55.74&coord_y=37.6&comment=тест_коммент");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);
        if ($server_output == "OK") { echo $server_output; } else { echo $server_output; }*/


       
        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"http://armageddont/web/apipushas");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "device_token=test&platform=1");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);
        if ($server_output == "OK") { echo $server_output; } else { echo $server_output; }*/

        //test create event_photos

        /*$time = time();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"http://armageddont/web/apitypes");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "type_name=tessttttypeeeee&type_image=w450h4001385925290Cloud.png&type_image_url=http://s1.iconbird.com/ico/2013/12/505/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);
        if ($server_output == "OK") { echo $server_output; } else { echo $server_output; }*/

        //apiphotos
        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"http://armageddont/web/apiphotos");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "event_id=14&photo_name=w450h4001385925290Cloud.png&photo_url=http://s1.iconbird.com/ico/2013/12/505/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);
        if ($server_output == "OK") { echo $server_output; } else { echo $server_output; }*/



        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"http://backend.armageddont.kingbird.ru/apimessages");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "event_id=14&created_at=1499167680&user_avatar=https://yt3.ggpht.com/-cMcGrq7WFbM/AAAAAAAAAAI/AAAAAAAAAAA/GbRTISXt768/s100-c-k-no-mo-rj-c0xffffff/photo.jpg&user=Колян&msg=Да....");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);
        if ($server_output == "OK") { echo $server_output; } else { echo $server_output; }*/

      
    }

    public function beforeAction($action)
    {            
        if ($action->id == 'deleteimage') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }
   

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDeleteimage()
    {
        if (Yii::$app->request->isAjax) {
        $data = Yii::$app->request->post();
        $photoId= $data['photoId'];
        $eventId= $data['eventId'];

        $eventPhoto = EventPhoto::findOne($photoId);        
        $eventPhoto->delete(false);
        echo 'good';
        }
    }

    public function actionFileupload()
    {       
       //empty 
    }

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

       /* $qwe = 37.62695680957 - rand(1, 3)/1000;        
        echo  $qwe;
        die();*/
        $model = new Event();

        if ( $model->load(Yii::$app->request->post())) {



            $model->created_at = time();

            if ( $model->save() ) { 
                
                //polygon
                $x[1] = $model->coord_x + rand(1, 3)/1000;
                $y[1] = $model->coord_y + rand(1, 3)/1000;

                $x[2] = $model->coord_x - rand(1, 3)/1000;
                $y[2] = $model->coord_y + rand(1, 3)/1000;

                $x[3] = $model->coord_x - rand(1, 3)/1000;
                $y[3] = $model->coord_y - rand(1, 3)/1000;

                $x[4] = $model->coord_x + rand(1, 3)/1000;
                $y[4] = $model->coord_y - rand(1, 3)/1000;                
                
                for ($i = 1; $i <= 4; $i++) {
                    $polygon = new Polygon();
                    $polygon->event_id = $model->id;
                    $polygon->coord_x = $x[$i];
                    $polygon->coord_y = $y[$i];
                    $polygon->save(false);
                }

                $images = UploadedFile::getInstances( $model, 'imageFile');
                
                if(!empty($images)) {
                foreach ($images as $file) {

                    $imageModel = new EventPhoto();
                    $imageModel->photo_name = $file->baseName . '.' . $file->extension;
                    $ext = end((explode(".", $file->baseName . '.' . $file->extension)));
                    $imageModel->photo_name = Yii::$app->security->generateRandomString().".{$ext}";
                    $file->saveAs(Yii::$app->basePath . '/web/uploads/' . $imageModel->photo_name);
                    
                    $imageModel->event_id = $model->id;
                    $imageModel->save();
                    }
                }

                return $this->redirect(['view', 'id' => $model->id]); 
            }
            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);        

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()){                
                
                \Yii::$app
                ->db
                ->createCommand()
                ->delete('polygon', ['event_id' => $id])
                ->execute();
                //polygon  
                $x[1] = $model->coord_x + rand(1, 3)/1000;
                $y[1] = $model->coord_y + rand(1, 3)/1000;

                $x[2] = $model->coord_x - rand(1, 3)/1000;
                $y[2] = $model->coord_y + rand(1, 3)/1000;

                $x[3] = $model->coord_x - rand(1, 3)/1000;
                $y[3] = $model->coord_y - rand(1, 3)/1000;

                $x[4] = $model->coord_x + rand(1, 3)/1000;
                $y[4] = $model->coord_y - rand(1, 3)/1000;                
                
                for ($i = 1; $i <= 4; $i++) {
                    $polygon = new Polygon();
                    $polygon->event_id = $id;
                    $polygon->coord_x = $x[$i];
                    $polygon->coord_y = $y[$i];
                    $polygon->save(false);
                }

                $images = UploadedFile::getInstances( $model, 'imageFile');
                
                if(!empty($images)) {
                foreach ($images as $file) {

                    $imageModel = new EventPhoto();
                    $imageModel->photo_name = $file->baseName . '.' . $file->extension;
                    $ext = end((explode(".", $file->baseName . '.' . $file->extension)));
                    $imageModel->photo_name = Yii::$app->security->generateRandomString().".{$ext}";
                    $file->saveAs(Yii::$app->basePath . '/web/uploads/' . $imageModel->photo_name);
                    
                    $imageModel->event_id = $model->id;
                    $imageModel->save();
                    }
                }

            return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Event::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

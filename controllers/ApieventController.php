<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\Event;
use app\models\EventPhoto;
use yii\data\ActiveDataProvider;
use app\models\Polygon;
use app\models\Push;
use app\models\CronPush;
use yii\web\UploadedFile;

class ApieventController extends ActiveController
{
    public $modelClass = 'app\models\Event';



     public function afterAction($action, $result)
	{
		$result = parent::afterAction($action, $result);
				if( Yii::$app->controller->action->id == 'create' AND !empty($result['id'])) {
						
			            $result['danger_level'] = Event::danger_level($result['id']);


			   			//polygon
		                $x[1] = $result['coord_x'] + rand(1, 9)/1000;
		                $y[1] = $result['coord_y'] + rand(1, 9)/1000;

		                $x[2] = $result['coord_x'] - rand(1, 9)/1000;
		                $y[2] = $result['coord_y'] + rand(1, 9)/1000;

		                $x[3] = $result['coord_x'] - rand(1, 9)/1000;
		                $y[3] = $result['coord_y'] - rand(1, 9)/1000;

		                $x[4] = $result['coord_x'] + rand(1, 9)/1000;
		                $y[4] = $result['coord_y'] - rand(1, 9)/1000;               
		                
		                for ($i = 1; $i <= 4; $i++) {
		                    $polygon = new Polygon();
		                    $polygon->event_id = $result['id'];
		                    $polygon->coord_x = $x[$i];
		                    $polygon->coord_y = $y[$i];
		                    $polygon->save(false);
		                }

		                $result['event_polygons'] = Polygon::find()     
					     ->where(['event_id' => $result['id']])
					     ->all();

					    $pushmessage = '';
					    if( $result['type'] == 1) {$pushmessage='Fire nearby'; }
					    if( $result['type'] == 2) {$pushmessage='Congestion nearby'; }
					    if( $result['type'] == 3) {$pushmessage='Accident nearby'; }
					    if( $result['type'] == 4) {$pushmessage='Technological disaster nearby'; }
					    if( $result['type'] == 5) {$pushmessage='Crime nearby'; }


					    $cronpush = new CronPush();
					    $cronpush->event_id = $result['id'];
					    $cronpush->owner = 'none';
					    $cronpush->textpush = $pushmessage;
					    $cronpush->save();

						if(isset($_FILES['event_photos']['tmp_name'])){
						for ($i = 0; $i < count($_FILES['event_photos']['tmp_name']); $i++) {
						    echo $i;
						$putdata = fopen($_FILES['event_photos']['tmp_name'][$i], "r");
						      	$photoname = uniqid().'.jpg';
						        $filename = \Yii::getAlias('@webroot') . '/uploads/'. $photoname;
						        $fp = fopen($filename, "w");
						        while ($data = fread($putdata, 1024))
						          fwrite($fp, $data);       
						        fclose($fp);
						        fclose($putdata);

						        $m = new EventPhoto();
						        $m->event_id=$result['id'];
								$m->photo_name = $photoname;
								$m->save();  
						}

						$result['event_photos'] = EventPhoto::find()     
											     ->where(['event_id' => $result['id']])
											     ->all();
						}
      			}
        return $result;
	}




   public function actions(){
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    public function actionIndex(){
        $activeData = new ActiveDataProvider([
            'query' => Event::find(),
            'pagination' => false,
        ]);
        return $activeData;
    }
}

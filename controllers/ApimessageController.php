<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\UserMessage;
use yii\data\ActiveDataProvider;

class ApimessageController extends ActiveController
{
    public $modelClass = 'app\models\UserMessage';




     public function actions(){
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    public function actionIndex(){
        $activeData = new ActiveDataProvider([
            'query' => UserMessage::find(),
            'pagination' => [
                'defaultPageSize' => 10000,
            ],
        ]);
        return $activeData;
    }

/*
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    public function actionIndex($id){
        $messages = UserMessage::find()
        ->where(['event_id' => $id])    
        ->all();
        print_r($messages);


        return $messages;
    }
   */
}

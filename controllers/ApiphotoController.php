<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;

class ApiphotoController extends ActiveController
{
    public $modelClass = 'app\models\EventPhoto';


    public function beforeAction($event)
    {
        $params = Yii::$app->getRequest()->getBodyParams();

        //copy image CREATE action
        if( Yii::$app->controller->action->id == 'create' ) {
	        if( $params['event_id'] AND $params['photo_name']  AND $params['photo_url'] ) { 
	        	copy($params['photo_url'].$params['photo_name'], Yii::$app->basePath . '/web/uploads/' . $params['type_image']); 
	    	}
        }
        return parent::beforeAction($event);
    }
}

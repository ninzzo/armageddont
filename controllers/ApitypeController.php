<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;

class ApitypeController extends ActiveController
{
    public $modelClass = 'app\models\EventType';


    public function beforeAction($event)
    {
        $params = Yii::$app->getRequest()->getBodyParams();

        //copy image CREATE action
        if( Yii::$app->controller->action->id == 'create' ) {
	        if( $params['type_image_url'] AND $params['type_image']  AND $params['type_name'] ) { 
	        	copy($params['type_image_url'].$params['type_image'], Yii::$app->basePath . '/web/uploads/' . $params['type_image']); 
	    	}
        }
        return parent::beforeAction($event);
    }
   
}

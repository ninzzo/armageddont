<?php

namespace app\controllers;

use Yii;
use app\models\UserMessage;
use app\models\UserMessageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UsermessageController implements the CRUD actions for UserMessage model.
 */
class UsermessageController extends Controller
{
    public $layout = '@app/views/layouts/admin';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        'access' => [
        'class' => AccessControl::className(),                
        'rules' => [
            [
            'actions' => ['index'],
            'allow' => true,
            'roles' => ['admin'],
            ],
            [
            'actions' => ['view'],
            'allow' => true,
            'roles' => ['admin'],
            ],
            [
            'actions' => ['create'],
            'allow' => true,
            'roles' => ['admin'],
            ],
            [
            'actions' => ['update'],
            'allow' => true,
            'roles' => ['admin'],
            ],
            [
            'actions' => ['delete'],
            'allow' => true,
            'roles' => ['admin'],
            ],
            
        ],
        ],
        ];
    }

    /**
     * Lists all UserMessage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserMessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('/message/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserMessage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('/message/view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserMessage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserMessage();

        if ( $model->load(Yii::$app->request->post())) {

            $model->created_at = time();

            if ( $model->save() ) {                

                return $this->redirect(['/usermessage/view', 'id' => $model->id]); 
            }
            
        } else {
            return $this->render('/message/create', [
                'model' => $model,
            ]);
        }

    }

    /**
     * Updates an existing UserMessage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);             

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()){

            return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('/message/update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserMessage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserMessage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

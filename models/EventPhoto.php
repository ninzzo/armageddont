<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_photo".
 *
 * @property integer $id
 * @property integer $event_id
 * @property string $photo_name
 * @property integer $main
 */
class EventPhoto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'photo_name'], 'required'],
            [['event_id', 'main'], 'integer'],
            [['photo_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event ID',
            'photo_name' => 'Photo Name',
            'main' => 'Main',
        ];
    }



      public function fields()
    {
        return [
            
            'id','event_id','photo_name',
            'photo_url' => function () {
                return 'http://backend.armageddont.kingbird.ru/uploads/';
            },         
          
        ];
    }
}

<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\models\EventType;
use app\models\EventPhoto;
use app\models\Polygon;
use app\models\CronPush;
use app\models\Push;


/**
 * This is the model class for table "event".
 *
 * @property integer $id
 * @property integer $created_at
 * @property string $name
 * @property integer $type
 * @property string $coord_x
 * @property string $coord_y
 * @property string $comment
 */
class Event extends \yii\db\ActiveRecord
{   
    public $imageFile;
    public $coordinates;
    public $event_photos;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    public function getEventtype()
    {
        return $this->hasOne(EventType::className(), ['id' => 'type']);

    }

    public function getEventphoto()
    {
        return $this->hasMany(EventPhoto::className(), ['event_id' => 'id']);
    }

    public function getEventpolygon()
    {
        return $this->hasMany(Polygon::className(), ['event_id' => 'id']);
    }
   

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type', 'coord_x', 'coord_y', 'comment'], 'required'],
            [['danger_level', 'created_at', 'type'], 'integer'],            
            //[['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif, jpeg', 'maxFiles' => 10],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'maxFiles' => 10],
            [['device_token', 'name', 'coord_x', 'coord_y'], 'string', 'max' => 255],
            [['comment', 'coordinates'], 'string', 'max' => 1000],
	    //[['event_photos'], 'file', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Время создания',
            'imageFile' => 'Добавить картинку',
            'name' => 'Название события',
            'type' => 'Тип события',
            'coord_x' => 'Координата X',
            'coord_y' => 'Координата Y',
            'comment' => 'Комментарий',
            'coordinates' => 'Координаты',
            'danger_level' => 'Уровень опасности',            
            
        ];
    }


    public function fields()
    {
        return [
            
            'id','created_at','name','coord_x','coord_y','comment','danger_level','device_token',
            'event_type_name' => function () {
                if(!empty($this->eventtype->type_name)){ return $this->eventtype->type_name; } else {
                    return '';
                }
                
            },
            /*'event_type_image' => function () {
                return $this->eventtype->type_image;
            },
            'event_type_image_url' => function () {
                return 'http://backend.armageddont.kingbird.ru/uploads/';
            },*/
            'type',           


            'event_photos' => function () {
                return $this->eventphoto;
            },   
            'event_polygons' => function () {
                return $this->eventpolygon;
            },               
        ];
    }


    static function pushas($deviceToken, $message) {
        
        $passphrase = '';
        $pempath = Yii::$app->basePath . '/arm.pem';

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $pempath);
        //stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        $fp = stream_socket_client(
          'ssl://gateway.push.apple.com:2195', $err,
          $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)
          exit("Failed to connect: $err $errstr" . PHP_EOL);
        $body['aps'] = array(
          'alert' => $message,
          'sound' => 'default',
          'link_url' => 'testURL',
          'category' => "NEWS_CATEGORY",
          );
        $payload = json_encode($body);
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
        $result = fwrite($fp, $msg , strlen($msg ));
        fclose($fp);
    }


    static function danger_level($event_id) {
        $danger_level = rand(2, 3);
        \Yii::$app->db->createCommand("UPDATE event SET danger_level=$danger_level WHERE id=:id")
        ->bindValue(':id', $event_id)
        ->execute();
        return $danger_level;
    }

    

 
}

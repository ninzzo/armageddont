<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "polygon".
 *
 * @property integer $id
 * @property integer $event_id
 * @property string $coord_x
 * @property string $coord_y
 */
class Polygon extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'polygon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'coord_x', 'coord_y'], 'required'],
            [['event_id'], 'integer'],
            [['coord_x', 'coord_y'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event ID',
            'coord_x' => 'Coord X',
            'coord_y' => 'Coord Y',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cron_push".
 *
 * @property integer $id
 * @property integer $send
 * @property integer $event_id
 * @property string $owner
 * @property string $textpush
 */
class CronPush extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cron_push';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['send', 'event_id'], 'integer'],
            [['event_id', 'owner', 'textpush'], 'required'],
            [['owner', 'textpush'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'send' => 'Send',
            'event_id' => 'Event ID',
            'owner' => 'Owner',
            'textpush' => 'Textpush',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_type".
 *
 * @property integer $id
 * @property integer $event_id
 * @property string $type_name
 * @property string $type_image
 */
class EventType extends \yii\db\ActiveRecord
{

    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_name'], 'required'],
            [['imageFile'], 'file', 'skipOnEmpty' => true],
            [['event_id'], 'integer'],
            [['type_name', 'type_image'], 'string', 'max' => 255],
        ];
    }

    

    public function getImage()
    {                     
        return \Yii::$app->request->BaseUrl.'/uploads/'.$this->type_image;          
    }


    public function upload()
    {        
        if ($this->validate()) {

            $this->type_image = $this->imageFile->name;
            $ext = end((explode(".", $this->imageFile->name)));

            $this->type_image = Yii::$app->security->generateRandomString().".{$ext}";
            $path = Yii::$app->basePath . '/web/uploads/' . $this->type_image;
            $this->imageFile->saveAs($path);

            return $this->type_image;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event ID',
            'type_name' => 'Имя типа события',
            'type_image' => 'Картинка',
        ];
    }

      public function fields()
    {
        return [
            
            'id','type_name','type_image',
            'type_image_url' => function () {
                return 'http://backend.armageddont.kingbird.ru/uploads/';
            },         
          
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_message".
 *
 * @property integer $id
 * @property integer $event_id
 * @property integer $created_at
 * @property string $user
 * @property string $user_avatar
 * @property string $msg
 */
class UserMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'created_at', 'user', 'user_avatar', 'msg'], 'required'],
            [['event_id', 'created_at'], 'integer'],
            [['user', 'user_avatar'], 'string', 'max' => 255],
            [['msg'], 'string', 'max' => 2000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Событие',
            'created_at' => 'Время создания',
            'user' => 'Имя пользователя',
            'user_avatar' => 'Абсолютный путь к аватарке',
            'msg' => 'Сообщение',
        ];
    }
}

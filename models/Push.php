<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "push".
 *
 * @property integer $id
 * @property string $device_token
 * @property integer $platform
 */
class Push extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'push';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['device_token', 'platform'], 'required'],
            [['platform'], 'integer'],
            [['device_token'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_token' => 'Device Token',
            'platform' => 'Platform',
        ];
    }

      public function fields()
    {
        return [
            
            'id','platform','device_token',               
          
        ];
    }
}
